package ru.t1.zkovalenko.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.IUserOwnerRepository;
import ru.t1.zkovalenko.tm.enumerated.Sort;
import ru.t1.zkovalenko.tm.model.AbstractUserOwnerModel;
import ru.t1.zkovalenko.tm.service.AbstractService;

import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnerModel> extends IUserOwnerRepository<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
