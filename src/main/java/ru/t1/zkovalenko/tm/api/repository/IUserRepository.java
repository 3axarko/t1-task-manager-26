package ru.t1.zkovalenko.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.service.IPropertyService;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull User create(
            @NotNull IPropertyService propertyService,
            @NotNull String login,
            @NotNull String password);

    @NotNull User create(
            @NotNull IPropertyService propertyService,
            @NotNull String login,
            @NotNull String password,
            @NotNull String email);

    @NotNull User create(
            @NotNull IPropertyService propertyService,
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    );

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

}
